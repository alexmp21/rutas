import {Routes, RouterModule} from '@angular/router';
import { CargaComponent } from './components/carga/carga.component';
import { FotosComponent } from './components/fotos/fotos.component';
import { RUTAS_ROUTES } from './components/carga/rutas.routes';
import { CiudadesComponent } from './components/ciudades/ciudades.component';
import { CIUDADES_ROUTES } from './components/ciudades/ciudades.routes';
import { JugadoresComponent } from './components/jugadores/jugadores.component';
import { ConfiguracionesComponent } from './components/configuraciones/configuraciones.component';
import { ControlComponent } from './components/configuraciones/control/control.component';
import { ValidacionComponent } from './components/configuraciones/validacion/validacion.component';
import { ComprobantesComponent } from './components/configuraciones/comprobantes/comprobantes.component';



const RUTAS:Routes = [

    {path:'fotos', component:FotosComponent},
    {path:'jugadores', component:JugadoresComponent},
    {path:'control', component:ControlComponent},
    {path:'validaciones',component:ValidacionComponent},
    {path:'comprobantes',component:ComprobantesComponent},
    {path:'configuraciones', component:ConfiguracionesComponent }, 
    {path:'ciudades', component:CiudadesComponent,
     children: CIUDADES_ROUTES
    },
    {path:'rutas', component:CargaComponent,
     children: RUTAS_ROUTES
    },
    {path:'**', pathMatch:'full', redirectTo:'fotos'}
];

export const APP_ROUTES = RouterModule.forRoot(RUTAS,{useHash:true})