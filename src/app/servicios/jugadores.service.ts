import {Injectable} from '@angular/core';

@Injectable()
export class JugadoresService{

private Jugadores:AtriJugador[] = [
    {
        posicion:"Delantero",
        Nombre:"Cristiano Ronal",
        Talla:"1.95mts",
        Origen:"Portugal",
        Club:"Juventus",
        Edad:"32"
    },
    {
        posicion:"Defensa",
        Nombre:"Sergio Ramos",
        Talla:"1.94mts",
        Origen:"España",
        Club:"Real Madrid",
        Edad:"33"
    },
    {
        posicion:"Volante de Ataque Derecho",
        Nombre:"Carrillo",
        Talla:"1.87mts",
        Origen:"Perú",
        Club:"Arabia",
        Edad:"34"
    },
    {
        posicion:"Delantero",
        Nombre:"Cristiano Ronal",
        Talla:"1.95mts",
        Origen:"Portugal",
        Club:"Juventus",
        Edad:"32"
    },
    {
        posicion:"Delantero",
        Nombre:"Cristiano Ronal",
        Talla:"1.95mts",
        Origen:"Portugal",
        Club:"Juventus",
        Edad:"32"
    },
    {
        posicion:"Delantero",
        Nombre:"Cristiano Ronal",
        Talla:"1.95mts",
        Origen:"Portugal",
        Club:"Juventus",
        Edad:"32"
    },
    {
        posicion:"Delantero",
        Nombre:"Cristiano Ronal",
        Talla:"1.95mts",
        Origen:"Portugal",
        Club:"Juventus",
        Edad:"32"
    },
    {
        posicion:"Delantero",
        Nombre:"Cristiano Ronal",
        Talla:"1.95mts",
        Origen:"Portugal",
        Club:"Juventus",
        Edad:"32"
    },
    {
        posicion:"Delantero",
        Nombre:"Cristiano Ronal",
        Talla:"1.95mts",
        Origen:"Portugal",
        Club:"Juventus",
        Edad:"32"
    },
    {
        posicion:"Delantero",
        Nombre:"Cristiano Ronal",
        Talla:"1.95mts",
        Origen:"Portugal",
        Club:"Juventus",
        Edad:"32"
    },
    {
        posicion:"Delantero",
        Nombre:"Cristiano Ronal",
        Talla:"1.95mts",
        Origen:"Portugal",
        Club:"Juventus",
        Edad:"32"
    },
    
]

constructor(){
    console.log('Los Mejores Jugadores en mi Lista')
}

getJugadores():AtriJugador[]{
    return this.Jugadores
}

}

export interface AtriJugador {
    posicion:string;
    Nombre:string;
    Talla:string;
    Origen:string;
    Club:string;
    Edad:string;
}

