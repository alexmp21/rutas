import { Component, OnInit } from '@angular/core';
import {JugadoresService, AtriJugador} from '../../servicios/jugadores.service';

@Component({
  selector: 'app-jugadores',
  templateUrl: './jugadores.component.html',
  styles: [
  ]
})
export class JugadoresComponent implements OnInit {
  
  jugador:AtriJugador[]=[];
  constructor( private _jugadores:JugadoresService ) {

   }
  

  ngOnInit(): void {
    this.jugador = this._jugadores.getJugadores();
    
  }

}
