import { Routes } from '@angular/router';
import { ArequipaComponent } from './arequipa/arequipa.component';
import { CuscoComponent } from './cusco/cusco.component';
import { TrujilloComponent } from './trujillo/trujillo.component';

export const CIUDADES_ROUTES:Routes = [
    {path:'trujillo', component:TrujilloComponent},
    {path:'arequipa', component:ArequipaComponent},
    {path:'cusco', component:CuscoComponent},
]
