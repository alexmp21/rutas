import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrujilloComponent } from './trujillo.component';

describe('TrujilloComponent', () => {
  let component: TrujilloComponent;
  let fixture: ComponentFixture<TrujilloComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrujilloComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrujilloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
