import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CuscoComponent } from './cusco.component';

describe('CuscoComponent', () => {
  let component: CuscoComponent;
  let fixture: ComponentFixture<CuscoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CuscoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CuscoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
