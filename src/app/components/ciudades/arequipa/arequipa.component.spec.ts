import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArequipaComponent } from './arequipa.component';

describe('ArequipaComponent', () => {
  let component: ArequipaComponent;
  let fixture: ComponentFixture<ArequipaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArequipaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArequipaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
