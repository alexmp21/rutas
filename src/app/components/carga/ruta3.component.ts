import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router'
@Component({
  selector: 'app-ruta3',
  template: `
    <p>
      ruta3 works!
    </p>
  `,
  styles: [
  ]
})
export class Ruta3Component implements OnInit {

  constructor( private activatedroute:ActivatedRoute) {
    this.activatedroute.params.subscribe(parametros=>{
      console.log('Rutas HIJAS Ruta 3 ')
      console.log(parametros)
    })
   }

  ngOnInit(): void {
  }

}
