import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-carga',
  templateUrl: './carga.component.html',
  styles: [
  ]
})
export class CargaComponent implements OnInit {

  constructor( private _activatedRoute:ActivatedRoute) {
      this._activatedRoute.parent.params.subscribe(parametros=>{
        console.log("Ruta PADRE")
        console.log(parametros)
      })
   }
  
  ngOnInit(): void {
  }

}
