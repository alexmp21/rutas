import { Routes} from '@angular/router';
import { Ruta1Component } from './ruta1.component';
import { Ruta2Component } from './ruta2.component';
import { Ruta3Component } from './ruta3.component';

export const RUTAS_ROUTES:Routes = [
    {path:'ruta1',component:Ruta1Component} ,
    {path:'ruta2',component:Ruta2Component},
    {path:'ruta3',component:Ruta3Component},
    {path:'**',pathMatch:'full', redirectTo:''}
]

