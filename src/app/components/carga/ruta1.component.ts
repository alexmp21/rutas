import { Component, OnInit } from '@angular/core';
import{ ActivatedRoute } from '@angular/router'
@Component({
  selector: 'app-ruta1',
  template: `
    <p>
      ruta1 works!
    </p>
  `,
  styles: [
  ]
})
export class Ruta1Component implements OnInit {

  constructor( private _activateroute:ActivatedRoute) {
    this._activateroute.params.subscribe(parametros=>{
      console.log('Rutas HIJAS Ruta 1')
      console.log(parametros)
    })
   }

  ngOnInit(): void {
  }

}
