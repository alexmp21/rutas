import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
@Component({
  selector: 'app-ruta2',
  template: `
    <p>
      ruta2 works!
    </p>
  `,
  styles: [
  ]
})
export class Ruta2Component implements OnInit {

  constructor( private _activatedroute:ActivatedRoute) { 
    this._activatedroute.params.subscribe(parametros=>{
      console.log('Rutas HIJAS Ruta 2')
      console.log(parametros);
    
    })
  }

  ngOnInit(): void {
  }

}
