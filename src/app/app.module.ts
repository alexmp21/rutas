import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//servicios
import {JugadoresService} from './servicios/jugadores.service'

//Rutas
import {APP_ROUTES} from './app.routes';

//Componenetes
import { AppComponent } from './app.component';
import { FotosComponent } from './components/fotos/fotos.component';
import { CargaComponent } from './components/carga/carga.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { Ruta1Component } from './components/carga/ruta1.component';
import { Ruta2Component } from './components/carga/ruta2.component';
import { Ruta3Component } from './components/carga/ruta3.component';
import { CiudadesComponent } from './components/ciudades/ciudades.component';
import { TrujilloComponent } from './components/ciudades/trujillo/trujillo.component';
import { ArequipaComponent } from './components/ciudades/arequipa/arequipa.component';
import { CuscoComponent } from './components/ciudades/cusco/cusco.component';
import { JugadoresComponent } from './components/jugadores/jugadores.component';
import { ConfiguracionesComponent } from './components/configuraciones/configuraciones.component';
import { ComprobantesComponent } from './components/configuraciones/comprobantes/comprobantes.component';
import { ValidacionComponent } from './components/configuraciones/validacion/validacion.component';
import { ControlComponent } from './components/configuraciones/control/control.component';

@NgModule({
  declarations: [
    AppComponent,
    FotosComponent,
    CargaComponent,
    NavbarComponent,
    Ruta1Component,
    Ruta2Component,
    Ruta3Component,
    CiudadesComponent,
    TrujilloComponent,
    ArequipaComponent,
    CuscoComponent,
    JugadoresComponent,
    ConfiguracionesComponent,
    ComprobantesComponent,
    ValidacionComponent,
    ControlComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTES,
  ],
  providers: [
    JugadoresService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
